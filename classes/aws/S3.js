"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

require("core-js");

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _mimeTypes = require("mime-types");

var _mimeTypes2 = _interopRequireDefault(_mimeTypes);

var _indexJs = require("./index.js");

var _indexJs2 = _interopRequireDefault(_indexJs);

var S3 = (function () {
    function S3() {
        _classCallCheck(this, S3);

        this.s3 = new _indexJs2["default"].S3();
        return this;
    }

    /**
     * S3 から key で指定した object を get し、その値が含まれる Promise をかえす。
     * 失敗すると Promise.reject() する。
     *
     * @param key
     * @param bucket
     * @returns {Promise}
     */

    _createClass(S3, [{
        key: "get",
        value: function get(key, bucket) {
            var _this = this;

            return new Promise(function (resolve, reject) {
                console.log("[get] " + S3.uriString(bucket, key));
                _this.s3.getObject({ Bucket: bucket, Key: key }, function (error, data) {
                    if (error) {
                        console.log("Error getting " + S3.uriString(bucket, key));
                        console.log(error, error.stack);
                        return reject(error);
                    } else {
                        return resolve(data.Body);
                    }
                });
            });
        }

        /**
         * S3 から keys で指定したすべての object を get し、その値を含む Map が含まれる Promise をかえす。
         * 一つでも失敗すると Promise.reject() する。
         *
         * @param keys []
         * @param bucket
         * @returns {Promise}
         */
    }, {
        key: "getAll",
        value: function getAll(keys, bucket) {
            var _this2 = this;

            return new Promise(function (resolve, reject) {
                console.log("[getAll]", keys);
                var map = new Map();
                return Promise.all(keys.map(function (key) {
                    return _this2.get(key, bucket).then(function (buffer) {
                        return map.set(key, buffer);
                    })["catch"](function (error) {
                        return reject(error);
                    });
                })).then(function () {
                    return resolve(map);
                })["catch"](function (error) {
                    return reject(error);
                });
            });
        }

        /**
         * S3 へ指定した key で put し、結果を含む Promise をかえす。
         * 失敗すると Promise.reject() する。
         *
         * @param body
         * @param key
         * @param bucket
         * @returns {Promise}
         */
    }, {
        key: "put",
        value: function put(body, key, bucket) {
            var _this3 = this;

            return new Promise(function (resolve, reject) {
                console.log("[put] " + S3.uriString(bucket, key));
                _this3.s3.upload({
                    Bucket: bucket,
                    Key: key,
                    Body: body,
                    ContentType: _mimeTypes2["default"].contentType(_path2["default"].extname(key))
                }, function (error, data) {
                    if (error) {
                        console.log(error, error.stack, data);
                        return reject(error);
                    } else {
                        return resolve("Success put " + key + " to " + bucket + ".");
                    }
                });
            });
        }

        /**
         * S3 上に指定した key が存在しているかを調べる。boolean を含む Promise をかえす。
         *
         * @param key
         * @param bucket
         * @returns {Promise}
         */
    }, {
        key: "exists",
        value: function exists(key, bucket) {
            var _this4 = this;

            return new Promise(function (resolve) {
                _this4.s3.headObject({ Bucket: bucket, Key: key }, function (error) {
                    // TODO: エラー処理
                    resolve(!error);
                });
            });
        }

        /**
         * 指定した Prefix, bucket に合致する key を__すべて__含んだ Set をかえす。
         * aws-sdk の listObjects では 1000 件までしか取得できないので、1000 件以上一度に取得したい場合使用する。
         *
         * @param prefix
         * @param bucket
         * @returns {Promise}  key の Set を含む。
         */
    }, {
        key: "listKeys",
        value: function listKeys(prefix, bucket) {
            var _this5 = this;

            console.log("[listKeys]", S3.uriString(bucket, prefix));
            return new Promise(function (resolve, reject) {
                var keys = [];
                var list = function list(innerPrefix, innerBucket) {
                    var marker = arguments.length <= 2 || arguments[2] === undefined ? "" : arguments[2];

                    _this5.s3.listObjects({
                        Prefix: innerPrefix,
                        Bucket: innerBucket,
                        Marker: marker
                    }, function (error, data) {
                        if (error) {
                            reject(error);
                        }
                        data.Contents.forEach(function (content) {
                            return keys.push(content.Key);
                        });
                        if (data.IsTruncated) {
                            list(innerPrefix, innerBucket, keys[keys.length - 1]);
                        } else {
                            resolve(new Set(keys));
                        }
                    });
                };
                list(prefix, bucket);
            });
        }

        /**
         * S3 uri を作ってかえす。
         *
         * @param bucket
         * @param key
         * @returns string
         */
    }], [{
        key: "uriString",
        value: function uriString(bucket, key) {
            return "s3://" + bucket + "/" + key;
        }
    }]);

    return S3;
})();

exports["default"] = S3;
module.exports = exports["default"];