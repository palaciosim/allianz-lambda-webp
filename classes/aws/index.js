"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _awsSdk = require("aws-sdk");

var _awsSdk2 = _interopRequireDefault(_awsSdk);

var _configJs = require("../../config.js");

var _configJs2 = _interopRequireDefault(_configJs);

_awsSdk2["default"].config.region = _configJs2["default"].aws.region;

if ("accessKeyId" in _configJs2["default"].aws && "secretAccessKey" in _configJs2["default"].aws) {
    _awsSdk2["default"].config.update({
        accessKeyId: _configJs2["default"].aws.accessKeyId,
        secretAccessKey: _configJs2["default"].aws.secretAccessKey
    });
}

exports["default"] = _awsSdk2["default"];
module.exports = exports["default"];