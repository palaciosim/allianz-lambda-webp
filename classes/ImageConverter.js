"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

require("core-js");

var _cwebp = require("cwebp");

var _configJs = require("../config.js");

var _configJs2 = _interopRequireDefault(_configJs);

if (!_fs2["default"].existsSync("/usr/local/bin/cwebp")) {
    var RESOURCES_DIR = _path2["default"].join(__dirname, "resources");
    process.env.PATH += ":" + RESOURCES_DIR;
    process.env.LD_LIBRARY_PATH += ":" + RESOURCES_DIR;
}

var ImageConverter = (function () {
    function ImageConverter(data) {
        _classCallCheck(this, ImageConverter);

        this.data = data;
        return this;
    }

    _createClass(ImageConverter, [{
        key: "getWebpAsync",
        value: function getWebpAsync() {
            var _this = this;

            var quality = arguments.length <= 0 || arguments[0] === undefined ? _configJs2["default"].image.quality : arguments[0];
            var preset = arguments.length <= 1 || arguments[1] === undefined ? _configJs2["default"].image.preset : arguments[1];

            return new Promise(function (resolve, reject) {
                var webp = new _cwebp.CWebp(_this.data);
                webp.quality(quality);
                webp.preset(preset);
                webp.toBuffer().then(function (buffer) {
                    return resolve(buffer);
                })["catch"](function (error) {
                    return reject(error);
                });
            });
        }
    }]);

    return ImageConverter;
})();

exports["default"] = ImageConverter;
;
module.exports = exports["default"];