"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.handler = handler;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

require("core-js");

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _classesAwsS3Js = require("./classes/aws/S3.js");

var _classesAwsS3Js2 = _interopRequireDefault(_classesAwsS3Js);

var _classesImageConverterJs = require("./classes/ImageConverter.js");

var _classesImageConverterJs2 = _interopRequireDefault(_classesImageConverterJs);

var _configJs = require("./config.js");

var _configJs2 = _interopRequireDefault(_configJs);

function handler(event, context) {
    var bucket = event.Records[0].s3.bucket.name;
    var key = event.Records[0].s3.object.key;
    var keyExt = _path2["default"].extname(key);
    var keyWebp = key.replace(keyExt, ".webp");

    // check if key triggered event is image
    if (_configJs2["default"].image.source.extentions.indexOf(keyExt.slice(1)) == -1) {
        context.fail(key + " seems not to be image file.");
        return;
    } else {
        console.log("Start converting to WebP. " + _classesAwsS3Js2["default"].uriString(bucket, key));
    }

    var s3 = new _classesAwsS3Js2["default"]();

    return s3.get(key, bucket).then(function (data) {
        return new _classesImageConverterJs2["default"](data);
    }).then(function (ic) {
        return ic.getWebpAsync();
    }).then(function (webp) {
        return s3.put(webp, keyWebp, bucket);
    }).then(function () {
        return context.succeed("Succeed. " + _classesAwsS3Js2["default"].uriString(bucket, keyWebp));
    })["catch"](function (error) {
        return context.fail(error);
    });
}